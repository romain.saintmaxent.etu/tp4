<img src="images/readme/header.jpg">

## Objectifs
- Savoir charger des ressources distantes avec AJAX
- Générer un code HTML en fonction de la valeur retournée par un appel AJAX
- Connecter un formulaire à un webservice distant

## Sommaire
Pour plus de clarté, les instructions du TP se trouvent dans des fichiers distincts (un fichier par sujet), procédez dans l'ordre sinon ça fonctionnera beaucoup moins bien ! :

1. [A. Préparatifs](A-preparatifs.md)
2. [B. AJAX : requêtes GET](B-ajax.md)
3. [C. AJAX : POST & formulaires](C-post.md)